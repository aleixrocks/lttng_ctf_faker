main: main.c
	gcc $^ -o $@

clean:
	rm -f main trace_cpu0 trace_cpu1

run: main
	./main

trace: run

lttng: trace
	mkdir -p fake/kernel
	cp ./metadata fake/kernel
	cp ./trace_cpu0 fake/kernel/channel_0
	cp ./trace_cpu1 fake/kernel/channel_1
