#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


int mk_packet_header(char *buf, size_t *len, const size_t bs)
{
	struct __attribute__((__packed__)) packet_header {
		uint32_t magic;
		uint32_t stream_id;
	};

	const int pks = sizeof(struct packet_header);
	struct packet_header *pk;

	if (*len + pks >= bs)
		return 1;

	pk = (struct packet_header *) &buf[*len];
	*pk = (struct packet_header) {
		.magic = 0xc1fc1fc1,
		.stream_id = 0
	};

	*len += pks;

	return 0;
}

int mk_packet_context(char *buf, size_t *len, const size_t bs,
		      uint32_t cpu_id)
{
	struct __attribute__((__packed__)) packet_context {
		uint32_t cpu_id;
	};

	const int pks = sizeof(struct packet_context);
	struct packet_context *pk;

	if (*len + pks >= bs)
		return 1;

	pk = (struct packet_context *) &buf[*len];
	*pk = (struct packet_context) {
		.cpu_id = cpu_id,
	};

	*len += pks;

	return 0;
}

struct __attribute__((__packed__)) event_header {
	uint32_t id;
	uint64_t timestamp;
};

uint64_t get_timestamp(void)
{
	static uint64_t ts = 0;

	ts += 1000;

	return ts;
}

int mk_event_header(struct event_header *pk, uint32_t id)
{
	*pk = (struct event_header) {
		.id = id,
		.timestamp = get_timestamp()
	};

	return 0;
}

size_t mk_sched_switch(char *buf, size_t *len, const size_t bs,
		       unsigned char *prev_comm,
		       int32_t prev_tid, int32_t prev_prio, int64_t prev_state,
		       unsigned char *next_comm,
		       int32_t next_tid, int32_t next_prio)
{
	struct __attribute__((__packed__)) sched_switch {
		struct event_header event_header;
		unsigned char prev_comm[16];
		int32_t prev_tid;
		int32_t prev_prio;
		int64_t prev_state;
		unsigned char next_comm[16];
		int32_t next_tid;
		int32_t next_prio;
	};

	const uint32_t id = 0;
	const int pks = sizeof(struct sched_switch);
	struct sched_switch *pk;
	int i;

	if (*len + pks >= bs)
		return 1;

	pk = (struct sched_switch *) &buf[*len];

	*pk = (struct sched_switch) {
		.prev_tid   = prev_tid,
		.prev_prio  = prev_prio,
		.prev_state = prev_state,
		.next_tid   = next_tid,
		.next_prio  = next_prio
	};

	mk_event_header(&pk->event_header, id);

	// prev_comm
	for (i = 0; i < sizeof(pk->prev_comm) && prev_comm[i]; i++)
		pk->prev_comm[i] = prev_comm[i];
	for (; i < sizeof(pk->prev_comm); i++)
		pk->prev_comm[i] = '\0';
	// next_comm
	for (i = 0; i < sizeof(pk->next_comm) && next_comm[i]; i++)
		pk->next_comm[i] = next_comm[i];
	for (; i < sizeof(pk->next_comm); i++)
		pk->next_comm[i] = '\0';

	*len += pks;

	return 0;
}

void trace_write(char *name, char *buf, size_t len)
{
	int fd;
	ssize_t ret;
	fd = open(name, O_WRONLY | O_CREAT | O_TRUNC, 0666);
	if (fd == -1) {
		perror("Error: cannot open output file");
		exit(EXIT_FAILURE);
	}

	ret = write(fd, buf, len);
	if (ret != len) {
		perror("Error: writing buffer");
		exit(EXIT_FAILURE);
	}
}

int main(int argc, char *argv[])
{
	char *buf;
	size_t len;
	const size_t bs = 8*4096;

	buf = malloc(bs);
	if (!buf) {
		perror("Error: malloc buffer");
		exit(EXIT_FAILURE);
	}

	len = 0;
	mk_packet_header(buf, &len, bs);
	mk_packet_context(buf, &len, bs, 0);
	mk_sched_switch(buf, &len, bs,
			"egg",     0x1111, 0x1, 0x1,
			"chicken", 0x2222, 0x1);
	mk_sched_switch(buf, &len, bs,
			"chicken", 0x2222, 0x1, 0x1,
			"egg",     0x1111, 0x1);
	mk_sched_switch(buf, &len, bs,
			"egg",     0x1111, 0x1, 0x1,
			"chicken", 0x2222, 0x1);
	mk_sched_switch(buf, &len, bs,
			"chicken", 0x2222, 0x1, 0x1,
			"egg",     0x1111, 0x1);
	trace_write("trace_cpu0", buf, len);

	len = 0;
	mk_packet_header(buf, &len, bs);
	mk_packet_context(buf, &len, bs, 1);
	mk_sched_switch(buf, &len, bs,
			"tako", 0x3333, 0x1, 0x1,
			"yaki", 0x4444, 0x1);
	mk_sched_switch(buf, &len, bs,
			"yaki", 0x4444, 0x1, 0x1,
			"tako", 0x3333, 0x1);
	mk_sched_switch(buf, &len, bs,
			"tako", 0x3333, 0x1, 0x1,
			"yaki", 0x4444, 0x1);
	mk_sched_switch(buf, &len, bs,
			"yaki", 0x4444, 0x1, 0x1,
			"tako", 0x3333, 0x1);
	trace_write("trace_cpu1", buf, len);

	return 0;
}
