/* CTF 1.8 */

typealias integer { size = 8; align = 8; signed = false; }  := uint8_t;
typealias integer { size = 16; align = 8; signed = false; } := uint16_t;
typealias integer { size = 32; align = 8; signed = false; } := uint32_t;
typealias integer { size = 64; align = 8; signed = false; } := uint64_t;
typealias integer { size = 64; align = 8; signed = false; } := unsigned long;
typealias integer { size = 5; align = 1; signed = false; }  := uint5_t;
typealias integer { size = 27; align = 1; signed = false; } := uint27_t;

trace {
	major = 1;
	minor = 8;
	byte_order = le;
	packet.header := struct {
		uint32_t magic;
		uint32_t stream_id;
	};
};

env {
	domain = "kernel";
};

clock {
	name = "monotonic";
	description = "Monotonic Clock";
	freq = 1000000000; /* Frequency, in Hz */
	/* clock value offset from Epoch is: offset * (1/freq) */
	offset = 1578378831114078890;
};

typealias integer {
	size = 64;
	align = 8;
	signed = false;
	map = clock.monotonic.value;
} := uint64_clock_monotonic_t;

stream {
	id = 0;
	packet.context := struct {
		uint32_t cpu_id;
	};
	event.header := struct {
		uint32_t id;
		uint64_clock_monotonic_t timestamp;
	};
};

event {
	name = "sched_switch";
	id = 0;
	stream_id = 0;
	fields := struct {
		integer { size = 8; align = 8; signed = 0; encoding = UTF8; base = 10; } _prev_comm[16];
		integer { size = 32; align = 8; signed = 1; encoding = none; base = 10; } _prev_tid;
		integer { size = 32; align = 8; signed = 1; encoding = none; base = 10; } _prev_prio;
		integer { size = 64; align = 8; signed = 1; encoding = none; base = 10; } _prev_state;
		integer { size = 8; align = 8; signed = 0; encoding = UTF8; base = 10; } _next_comm[16];
		integer { size = 32; align = 8; signed = 1; encoding = none; base = 10; } _next_tid;
		integer { size = 32; align = 8; signed = 1; encoding = none; base = 10; } _next_prio;
	};
};
